package ru.t1.simanov.tm.command.system;

import ru.t1.simanov.tm.api.service.ICommandService;
import ru.t1.simanov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }


}
