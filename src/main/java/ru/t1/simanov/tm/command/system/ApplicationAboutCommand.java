package ru.t1.simanov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Show developer info.";

    public static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Dima Simanov");
        System.out.println("E-mail: simanov.dima@gmail.com");
        System.out.println("E-mail: dsimanov@t1-consulting.ru");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
