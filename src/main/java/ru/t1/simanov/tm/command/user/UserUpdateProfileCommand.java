package ru.t1.simanov.tm.command.user;

import ru.t1.simanov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Update profile of current user.";

    public static final String NAME = "user-update-profile";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER PROFILE UPDATE]");
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
