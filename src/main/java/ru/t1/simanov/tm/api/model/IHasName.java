package ru.t1.simanov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
