package ru.t1.simanov.tm.repository;

import ru.t1.simanov.tm.api.repository.ITaskRepository;
import ru.t1.simanov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String name, final String description) {
        return add(new Task(name, description));
    }

    @Override
    public Task create(final String name) {
        return add(new Task(name));
    }

    public List<Task> findAllTasksByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
