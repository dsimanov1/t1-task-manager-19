package ru.t1.simanov.tm.repository;

import ru.t1.simanov.tm.api.repository.IProjectRepository;
import ru.t1.simanov.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String name, final String description) {
        return add(new Project(name, description));
    }

    @Override
    public Project create(final String name) {
        return add(new Project(name));
    }

}
