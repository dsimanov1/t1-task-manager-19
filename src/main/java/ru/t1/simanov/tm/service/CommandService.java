package ru.t1.simanov.tm.service;

import ru.t1.simanov.tm.api.repository.ICommandRepository;
import ru.t1.simanov.tm.api.service.ICommandService;
import ru.t1.simanov.tm.command.AbstractCommand;
import ru.t1.simanov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.simanov.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (argument == null || argument.isEmpty()) throw new ArgumentNotSupportedException();
        AbstractCommand command = commandRepository.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException();
        return command;
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        AbstractCommand command = commandRepository.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException();
        return command;
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
